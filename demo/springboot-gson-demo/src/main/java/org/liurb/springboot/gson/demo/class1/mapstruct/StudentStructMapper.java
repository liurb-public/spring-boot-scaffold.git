package org.liurb.springboot.gson.demo.class1.mapstruct;

import org.liurb.springboot.gson.demo.class1.entity.Student;
import org.liurb.springboot.gson.demo.class1.vo.StudentHomeVo;
import org.liurb.springboot.gson.demo.class1.vo.StudentPageVo;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

/**
 * 学生实体转换接口
 *
 * 定义这是一个MapStruct对象属性转换接口，在这个类里面规定转换规则
 *
 * @Author Liurb
 * @Date 2022/11/30
 */
@Mapper
public interface StudentStructMapper {

    /**
     * 获取该类自动生成的实现类的实例
     *
     */
    StudentStructMapper INSTANCES = Mappers.getMapper(StudentStructMapper.class);

    /**
     * 这个方法就是用于实现对象属性复制的方法
     *
     * @Mapping 注解 用于定义属性复制规则
     * source 指定源对象属性
     * target指定目标对象属性
     *
     * @param student 这个参数就是源对象，也就是需要被复制的对象
     * @return 返回的是目标对象，就是最终的结果对象
     */
    @Mappings({
            @Mapping(source = "id", target = "userId"),
            @Mapping(source = "name", target = "userName")
    })
    StudentHomeVo toStudentHomeVo(Student student);

    /**
     * 也可以实现多个复制方法，一般将一个实体源对象的转换写在一起
     *
     * @param student
     * @return
     */
    @Mapping(source = "sex", target = "gender")
    StudentPageVo toStudentPageVo(Student student);
}
