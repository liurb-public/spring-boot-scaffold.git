package org.liurb.springboot.gson.demo.class1.controller;

import jakarta.annotation.Resource;
import org.liurb.springboot.gson.demo.class1.entity.Student;
import org.liurb.springboot.gson.demo.class1.mapstruct.StudentStructMapper;
import org.liurb.springboot.gson.demo.class1.service.StudentService;
import org.liurb.springboot.gson.demo.class1.vo.StudentHomeVo;
import org.liurb.springboot.gson.demo.class1.vo.StudentPageVo;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

/**
 * gson消息 控制器
 *
 * @Author Liurb
 * @Date 2022/11/30
 */
@RestController
@RequestMapping("/gson_demo_api/http_message")
public class GsonHttpMessageController {

    @Resource
    StudentService studentService;

    @PostMapping("/user")
    public String user(@RequestBody StudentHomeVo studentHomeVo) {

        return studentHomeVo.getUserName();
    }

    @GetMapping("/page")
    public List<StudentPageVo> page() {
        List<Student> students = studentService.list();

        List<StudentPageVo> studentPageVos = students.stream().map(item -> {

            StudentPageVo studentPageVo = StudentStructMapper.INSTANCES.toStudentPageVo(item);
            return studentPageVo;
        }).collect(Collectors.toList());

        return studentPageVos;
    }

}
