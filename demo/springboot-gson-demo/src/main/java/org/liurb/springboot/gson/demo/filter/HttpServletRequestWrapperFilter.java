package org.liurb.springboot.gson.demo.filter;

import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletRequest;
import jakarta.servlet.ServletResponse;
import jakarta.servlet.annotation.WebFilter;
import org.liurb.springboot.starter.web.mvc.filter.BackendHttpServletRequestFilter;


/**
 * 过滤器
 *
 * 实现对传入的 httpServletRequest 的转换
 *
 * @Author Liurb
 * @Date 2022/11/30
 */
@WebFilter(filterName = "httpServletRequestWrapperFilter", urlPatterns = {"/gson_demo_api/*"})
public class HttpServletRequestWrapperFilter extends BackendHttpServletRequestFilter {

    @Override
    public void doFilterMore(ServletRequest request, ServletResponse response, FilterChain chain) {

    }
}
