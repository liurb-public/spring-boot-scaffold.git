package org.liurb.springboot.gson.demo.class1.vo;

import com.google.gson.annotations.SerializedName;
import lombok.Data;
import org.liurb.springboot.starter.gson.annotation.FieldExclude;

/**
 * 学生分页vo
 *
 * @Author Liurb
 * @Date 2022/11/30
 */
@Data
public class StudentPageVo {

    private Integer id;
    @SerializedName("user_name")
    private String name;
    @FieldExclude
    private Integer age;

    private String gender;

}
