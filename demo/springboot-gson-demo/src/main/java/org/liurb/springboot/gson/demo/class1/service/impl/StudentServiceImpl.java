package org.liurb.springboot.gson.demo.class1.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import org.liurb.springboot.gson.demo.class1.entity.Student;
import org.liurb.springboot.gson.demo.class1.mapper.StudentMapper;
import org.liurb.springboot.gson.demo.class1.service.StudentService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 学生表 服务实现类
 * </p>
 *
 * @author Liurb
 * @since 2022-11-13
 */
@Service
public class StudentServiceImpl extends ServiceImpl<StudentMapper, Student> implements StudentService {

}
