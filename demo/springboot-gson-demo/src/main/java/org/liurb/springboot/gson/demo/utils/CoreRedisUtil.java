package org.liurb.springboot.gson.demo.utils;

import jakarta.annotation.Resource;
import org.liurb.springboot.starter.redis.utils.BaseRedisUtil;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;


/**
 * redis帮助类
 *
 * @Author Liurb
 * @Date 2022/11/30
 */
@Component
public class CoreRedisUtil extends BaseRedisUtil {

    @Resource(name = "redisTemplate")
    RedisTemplate<String, Object> redisTemplate;

    @Override
    public RedisTemplate<String, Object> getRedisTemplate() {

        return redisTemplate;
    }
}
