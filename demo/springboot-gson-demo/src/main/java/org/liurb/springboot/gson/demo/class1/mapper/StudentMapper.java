package org.liurb.springboot.gson.demo.class1.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.liurb.springboot.gson.demo.class1.entity.Student;

/**
 * <p>
 * 学生表 Mapper 接口
 * </p>
 *
 * @author Liurb
 * @since 2022-11-13
 */
public interface StudentMapper extends BaseMapper<Student> {

}
