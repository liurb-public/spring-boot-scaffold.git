package org.liurb.springboot.gson.demo.class1.vo;

import com.google.gson.annotations.SerializedName;
import lombok.Data;

/**
 * 学生首页vo
 *
 * @Author Liurb
 * @Date 2022/11/30
 */
@Data
public class StudentHomeVo {


    private Integer userId;

    @SerializedName("user_text")
    private String userName;

    private Integer age;

    private String sex;

}
