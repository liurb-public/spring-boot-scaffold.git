package org.liurb.springboot.advance.demo.class3;

import jakarta.annotation.Resource;
import org.junit.jupiter.api.Test;
import org.liurb.springboot.advance.demo.class3.service.MyRedisCacheService;
import org.liurb.springboot.advance.demo.class3.vo.StudentVo;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class Class3DemoApplicationTests {

    @Resource
    MyRedisCacheService myRedisCacheService;

    @Test
    void contextLoads() {

        StudentVo user = myRedisCacheService.getUser(1);
        System.out.println(user);

    }

}
