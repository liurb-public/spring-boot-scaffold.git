package org.liurb.springboot.advance.demo.class4;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import jakarta.annotation.Resource;
import org.junit.jupiter.api.Test;
import org.liurb.springboot.advance.demo.class4.entity.TUser;
import org.liurb.springboot.advance.demo.class4.service.TUserService;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;

import java.util.List;

/**
 * phone字段分表
 *
 * 哈希取模算法
 *
 * @Author LiuRuiBin
 * @Date 2023/3/4
 */
@SpringBootTest
@TestPropertySource(properties = {"spring.config.location=classpath:class4/application-mod-phone.yml"})
public class Class4ModPhoneDemoApplicationTests {

    @Resource
    TUserService tUserService;

    @Test
    void contextLoads() {

//        for (int i=0; i<100; i++) {
//            TUser user = new TUser();
//            user.setName("张" + i);
//            String age = RandomStringUtils.randomNumeric(2);
//            user.setAge(Integer.parseInt(age));
//            String phone = RandomStringUtils.randomNumeric(8);
//            user.setPhone("159" + phone);
//            tUserService.save(user);
//        }

//        List<TUser> list = tUserService.list(new LambdaQueryWrapper<TUser>().eq(TUser::getPhone, "15953790357"));
//        System.out.println(list);

        Page page = new Page(0, 10);

        Page pageData = tUserService.page(page, new LambdaQueryWrapper<TUser>().eq(TUser::getPhone, "15953790357"));
        List records = pageData.getRecords();
        System.out.println(records);



    }

}
