package org.liurb.springboot.advance.demo.class2;

import jakarta.annotation.Resource;
import org.junit.jupiter.api.Test;
import org.liurb.springboot.advance.demo.class2.handler.payment.dto.PayOrder;
import org.liurb.springboot.advance.demo.class2.handler.payment.dto.PayResult;
import org.liurb.springboot.advance.demo.class2.handler.payment.service.VendorPaymentService;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class Class2DemoApplicationTests {

    /**
     * 支付统一入口
     */
    @Resource
    VendorPaymentService vendorPaymentService;

    @Test
    void contextLoads() {

        PayOrder payOrder = new PayOrder();
        //微信支付方式
        payOrder.setChannel("wechat");
        payOrder.setMete("100");
        payOrder.setPhone("123456");

        PayResult payResult = vendorPaymentService.pay(payOrder);
        System.out.println(payResult);

    }

}
