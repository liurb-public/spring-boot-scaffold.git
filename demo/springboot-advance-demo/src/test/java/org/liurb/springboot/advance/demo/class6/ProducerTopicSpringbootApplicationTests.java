package org.liurb.springboot.advance.demo.class6;

import jakarta.annotation.Resource;
import org.junit.jupiter.api.Test;
import org.liurb.springboot.advance.demo.class6.conf.RabbitmqTopicConfig;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * Topic 模式生产者
 *
 * @Author LiuRuiBin
 * @Date 2023/8/10
 */
@SpringBootTest
public class ProducerTopicSpringbootApplicationTests {

    @Resource
    RabbitTemplate rabbitTemplate;

    @Test
    void contextLoads() {

        for (int i=0; i<100; i++) {
            //使用rabbitTemplate发送email消息
            String email = "send email " + i + " message to user";
            /**
             * 参数：
             * 1、交换机名称
             * 2、routingKey
             * 3、消息内容
             */
            rabbitTemplate.convertAndSend(RabbitmqTopicConfig.EXCHANGE_TOPICS_INFORM, "inform.email", email);

            //使用rabbitTemplate发送sms消息
            String sms = "send sms " + i + " message to user";
            /**
             * 参数：
             * 1、交换机名称
             * 2、routingKey
             * 3、消息内容
             */
            rabbitTemplate.convertAndSend(RabbitmqTopicConfig.EXCHANGE_TOPICS_INFORM, "inform.sms", sms);
        }


    }

}
