package org.liurb.springboot.advance.demo.class6;

import jakarta.annotation.Resource;
import org.junit.jupiter.api.Test;
import org.liurb.springboot.advance.demo.class6.conf.RabbitmqFanoutConfig;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * Fanout 模式生产者
 *
 * @Author LiuRuiBin
 * @Date 2023/8/10
 */
@SpringBootTest
public class ProducerFanoutSpringbootApplicationTests {

    @Resource
    RabbitTemplate rabbitTemplate;

    @Test
    void contextLoads() {

        for (int i=0; i<100; i++) {

            //使用rabbitTemplate 广播发送消息，pc端和app端的消费者都会接受到此消息
            String message = "send " + i + " message to user";
            /**
             * 参数：
             * 1、交换机名称
             * 2、routingKey
             * 3、消息内容
             */
            rabbitTemplate.convertAndSend(RabbitmqFanoutConfig.EXCHANGE_FANOUT_NOTICE, "", message);
        }


    }

}
