package org.liurb.springboot.advance.demo.class6;

import jakarta.annotation.Resource;
import org.junit.jupiter.api.Test;
import org.liurb.springboot.advance.demo.class6.conf.RabbitmqDeadConfig;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * 死信队列 生产者
 *
 * @Author LiuRuiBin
 * @Date 2023/8/10
 */
@SpringBootTest
public class ProducerDeadSpringbootApplicationTests {

    @Resource
    RabbitTemplate rabbitTemplate;

    @Test
    void contextLoads() {

        for (int i=0; i<10; i++) {

            //使用rabbitTemplate
            String message = "wx paying " + i ;

            /**
             * 参数：
             * 1、交换机名称
             * 2、routingKey
             * 3、消息内容
             * 4、消息配置
             */
            rabbitTemplate.convertAndSend(RabbitmqDeadConfig.EXCHANGE_NORMAL_PAYMENT, RabbitmqDeadConfig.ROUTING_KEY_PAYMENT_WX, message);

        }


    }

}
