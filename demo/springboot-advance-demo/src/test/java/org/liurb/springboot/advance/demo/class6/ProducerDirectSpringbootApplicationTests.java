package org.liurb.springboot.advance.demo.class6;

import jakarta.annotation.Resource;
import org.junit.jupiter.api.Test;
import org.liurb.springboot.advance.demo.class6.conf.RabbitmqDirectConfig;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Random;

/**
 * Direct 模式生产者
 *
 * @Author LiuRuiBin
 * @Date 2023/8/10
 */
@SpringBootTest
public class ProducerDirectSpringbootApplicationTests {

    @Resource
    RabbitTemplate rabbitTemplate;

    @Test
    void contextLoads() {

        for (int i=0; i<10; i++) {

            int priority = new Random().nextInt(0, 11);

            //使用rabbitTemplate
            String message = "card " + i + " is paying, priority is " + priority;

            /**
             * 参数：
             * 1、交换机名称
             * 2、routingKey
             * 3、消息内容
             * 4、消息配置
             */
            rabbitTemplate.convertAndSend(RabbitmqDirectConfig.EXCHANGE_DIRECT_PAYMENT, RabbitmqDirectConfig.ROUTING_KEY_CARD, message, msg -> {
                //设置优先级
                msg.getMessageProperties().setPriority(priority);
                //设置有效时间(毫秒)
//                msg.getMessageProperties().setExpiration(expiration + "");
                return msg;
            });

        }


    }

}
