package org.liurb.springboot.advance.demo.class7;

import co.elastic.clients.elasticsearch.ElasticsearchClient;
import jakarta.annotation.Resource;
import org.junit.jupiter.api.Test;
import org.liurb.springboot.advance.demo.class7.bean.EsBook;
import org.liurb.springboot.advance.demo.class7.repository.EsBookRepository;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @Author LiuRuiBin
 * @Date 2023/10/17
 */
@SpringBootTest
public class Class7ElasticSearchApplicationTests {

    @Resource
    ElasticsearchClient client;
    @Resource
    EsBookRepository bookRepository;

    /**
     * 使用Repository接口保存数据
     */
    @Test
    void repositoryCreate() {

        EsBook book = new EsBook();
        book.setId(1);
        book.setTitle("宋太祖");
        book.setAuthor("顾宏义");
        book.setType(1);
        book.setIntro("作为赵宋的开国皇帝，赵匡胤为世人所熟知的事迹，多与军事有关。然而本书不仅细腻地还原了黄袍加身、雪夜定策、杯酒释 兵权等重要历史现场，更详细梳理其作为一介出身平平的武夫，在一夕篡权之后，如何凭借极其敏锐的政治嗅觉，确立“尚文抑武”的国策，在南征北战的同时兴文教、重农商、布宽政、整吏治，不仅使宋朝免于重蹈唐末至五代武将专权之覆辙，也为后世三百余年奠定了“华夏文化登峰造极”的坚实基础。");
        book.setPublishTime(LocalDateTime.of(2023,9, 1, 0, 0, 0));
        bookRepository.save(book);
    }

    /**
     * 使用Repository接口查询数据
     */
    @Test
    void repositorySearch() {

        List<EsBook> esTitleBooks = bookRepository.findByTitle("宋太祖");
        System.out.println(esTitleBooks);

        List<EsBook> esIntroBooks = bookRepository.findByIntro("赵匡胤");
        System.out.println(esTitleBooks);

    }

    /**
     * 使用Repository接口删除数据
     */
    @Test
    void repositoryDel() {
        bookRepository.deleteAll();
    }

}
