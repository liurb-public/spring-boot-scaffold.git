package org.liurb.springboot.advance.demo.class4.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 
 * </p>
 *
 * @author Liurb
 * @since 2023-03-06
 */
@Getter
@Setter
@TableName("t_user")
public class TUser implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @TableField("app_id")
    private String appId;

    @TableField("open_id")
    private String openId;

    @TableField("name")
    private String name;

    @TableField("age")
    private Integer age;

    @TableField("phone")
    private String phone;
}
