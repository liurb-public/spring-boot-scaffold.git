package org.liurb.springboot.advance.demo.class6.handler;

import com.rabbitmq.client.Channel;
import org.liurb.springboot.advance.demo.class6.conf.RabbitmqDeadConfig;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

/**
 * 死信队列 模式消费者
 *
 * @Author LiuRuiBin
 * @Date 2023/8/10
 */
@ConditionalOnExpression("${mq.starter.config.enabled:true} && '${mq.starter.config.messaging}'.equals('rabbitmq_dead')")
@Component
public class RabbitMqDeadReceiveHandler {

    /**
     * 监听normal_wx队列
     *
     * @param msg
     * @param message
     * @param channel
     */
    @RabbitListener(queues = {RabbitmqDeadConfig.QUEUE_NORMAL_WX})
    public void receive_normal_wx(Object msg, Message message, Channel channel){
        System.out.println("QUEUE_NORMAL_WX msg："+msg);
        try {
            TimeUnit.SECONDS.sleep(5);//5秒
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 监听死信队列
     *
     * @param msg
     * @param message
     * @param channel
     */
    @RabbitListener(queues = {RabbitmqDeadConfig.QUEUE_DEAD_WX})
    public void receive_dead_wx(Object msg, Message message, Channel channel){
        System.out.println("QUEUE_DEAD_WX msg："+msg);
        try {
            TimeUnit.SECONDS.sleep(5);//5秒
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

}
