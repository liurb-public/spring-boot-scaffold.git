package org.liurb.springboot.advance.demo.class2.handler.payment.service.impl;

import org.liurb.springboot.advance.demo.class2.handler.payment.annotation.Payment;
import org.liurb.springboot.advance.demo.class2.handler.payment.dto.PayOrder;
import org.liurb.springboot.advance.demo.class2.handler.payment.dto.PayResult;
import org.liurb.springboot.advance.demo.class2.handler.payment.service.PaymentHandleService;
import org.springframework.stereotype.Service;

/**
 * 不支持的业务处理实现
 *
 * @Author Liurb
 * @Date 2022/11/26
 */
@Payment("nonsupport")
@Service("NonsupportPaymentHandleServiceImpl")
public class NonsupportPaymentHandleServiceImpl implements PaymentHandleService {

    @Override
    public PayResult pay(PayOrder payOrder) {
        PayResult result = new PayResult();
        result.setCode(-1);
        return result;
    }
}
