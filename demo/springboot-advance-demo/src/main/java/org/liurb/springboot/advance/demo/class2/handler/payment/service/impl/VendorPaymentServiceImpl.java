package org.liurb.springboot.advance.demo.class2.handler.payment.service.impl;

import jakarta.annotation.Resource;
import org.liurb.springboot.advance.demo.class2.handler.payment.dto.PayOrder;
import org.liurb.springboot.advance.demo.class2.handler.payment.dto.PayResult;
import org.liurb.springboot.advance.demo.class2.handler.payment.service.PaymentContextService;
import org.liurb.springboot.advance.demo.class2.handler.payment.service.PaymentHandleService;
import org.liurb.springboot.advance.demo.class2.handler.payment.service.VendorPaymentService;
import org.springframework.stereotype.Service;


/**
 * 支付处理服务统一入口
 *
 * @Author Liurb
 * @Date 2022/11/26
 */
@Service
public class VendorPaymentServiceImpl implements VendorPaymentService {

    @Resource
    PaymentContextService paymentContextService;

    @Override
    public PayResult pay(PayOrder payOrder) {
        //获取订单中的渠道
        String channel = payOrder.getChannel();
        //根据渠道，具体选择所使用的支付处理类
        PaymentHandleService handleService = paymentContextService.getContext(channel);
        //调用该支付处理类的支付方法
        return handleService.pay(payOrder);
    }

}
