package org.liurb.springboot.advance.demo.class3.vo;

import lombok.Data;

/**
 * 学生vo
 *
 * @Author Liurb
 * @Date 2022/12/3
 */
@Data
public class StudentVo {

    private Integer id;

    private String name;

    private Integer age;

    private String sex;

}
