package org.liurb.springboot.advance.demo.class3.service;

import org.liurb.springboot.advance.demo.class3.vo.StudentVo;

/**
 * redis测试服务
 *
 * @Author Liurb
 * @Date 2022/12/3
 */
public interface MyRedisCacheService {


    StudentVo getUser(int id);

    StudentVo getUser(StudentVo vo);

}
