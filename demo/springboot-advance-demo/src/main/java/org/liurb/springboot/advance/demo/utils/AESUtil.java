package org.liurb.springboot.advance.demo.utils;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.util.Base64;

/**
 * AES工具类
 *
 * @Author LiuRuiBin
 * @Date 2024/5/6
 */
public class AESUtil {

    private static final String ALGORITHM = "AES/CBC/PKCS5Padding";

    /**
     * 加密
     *
     * @param content
     * @param password
     * @param iv
     * @return
     * @throws Exception
     */
    public static String encrypt(String content, String password, String iv) throws Exception {
        SecretKeySpec keySpec = new SecretKeySpec(password.getBytes(), "AES");
        Cipher cipher = Cipher.getInstance(ALGORITHM);

        byte[] IV = iv.getBytes();

        cipher.init(Cipher.ENCRYPT_MODE, keySpec, new IvParameterSpec(IV));
        byte[] encrypted = cipher.doFinal(content.getBytes());
        return Base64.getEncoder().encodeToString(encrypted);
    }

    /**
     * 解密
     *
     * @param encryptedContent
     * @param password
     * @param iv
     * @return
     * @throws Exception
     */
    public static String decrypt(String encryptedContent, String password, String iv) throws Exception {
        SecretKeySpec keySpec = new SecretKeySpec(password.getBytes(), "AES");
        Cipher cipher = Cipher.getInstance(ALGORITHM);

        byte[] IV = iv.getBytes();

        cipher.init(Cipher.DECRYPT_MODE, keySpec, new IvParameterSpec(IV));
        byte[] decrypted = cipher.doFinal(Base64.getDecoder().decode(encryptedContent));
        return new String(decrypted);
    }

}
