package org.liurb.springboot.advance.demo.class1.vo;

import lombok.Data;

/**
 * 学生首页展示vo
 *
 *
 * @Author Liurb
 * @Date 2022/11/13
 */
@Data
public class StudentHomeVo {

    private Integer userId;

    private String userName;

    private Integer age;

    private String sex;

}
