package org.liurb.springboot.advance.demo.class3.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import jakarta.annotation.Resource;
import org.liurb.springboot.advance.demo.class1.entity.Student;
import org.liurb.springboot.advance.demo.class1.service.StudentService;
import org.liurb.springboot.advance.demo.class3.annotation.MyRedisCache;
import org.liurb.springboot.advance.demo.class3.service.MyRedisCacheService;
import org.liurb.springboot.advance.demo.class3.vo.StudentVo;
import org.springframework.stereotype.Service;

/**
 * redis测试服务
 *
 * @Author Liurb
 * @Date 2022/12/3
 */
@Service
public class MyRedisCacheServiceImpl implements MyRedisCacheService {

    @Resource
    StudentService studentService;

    @MyRedisCache(key = "'user:id:'+#id")
    @Override
    public StudentVo getUser(int id) {//缓存key使用参数用户id

        Student student = studentService.getById(id);

        if (student != null) {
            StudentVo vo = new StudentVo();
            vo.setId(student.getId());
            vo.setName(student.getName());
            vo.setAge(student.getAge());
            vo.setSex(student.getSex());
            return vo;
        }

        return null;
    }

    @MyRedisCache(key = "'user:name:'+#vo.name+':age:'+#vo.age")
    @Override
    public StudentVo getUser(StudentVo vo) {//缓存key使用参数vo的名称和年龄

        Student student = studentService.getOne(new LambdaQueryWrapper<Student>().eq(Student::getName, vo.getName()));
        if (student != null) {
            StudentVo studentVo = new StudentVo();
            studentVo.setId(student.getId());
            studentVo.setName(student.getName());
            studentVo.setAge(student.getAge());
            studentVo.setSex(student.getSex());
            return studentVo;
        }

        return null;
    }
}
