package org.liurb.springboot.advance.demo.class6.conf;

import org.springframework.amqp.core.*;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.context.annotation.Bean;

import java.util.HashMap;
import java.util.Map;

/**
 * rabbitmq 死信队列配置类
 *
 * @Author LiuRuiBin
 * @Date 2023/8/14
 */
@ConditionalOnExpression("${mq.starter.config.enabled:true} && '${mq.starter.config.messaging}'.equals('rabbitmq_dead')")
@AutoConfiguration
public class RabbitmqDeadConfig {


    /** 正常配置 */

    public static final String QUEUE_NORMAL_WX = "queue_normal_wx";
    public static final String EXCHANGE_NORMAL_PAYMENT = "exchange_normal_payment";
    public static final String ROUTING_KEY_PAYMENT_WX = "normal.payment.wx";

    /**
     * 声明交换机
     *
     * @return
     */
    @Bean(EXCHANGE_NORMAL_PAYMENT)
    public Exchange EXCHANGE_NORMAL_PAYMENT(){
        //durable(true) 持久化，mq重启之后交换机还在
        return ExchangeBuilder.directExchange(EXCHANGE_NORMAL_PAYMENT).durable(true).build();
    }

    /**
     * 声明QUEUE_NORMAL_WX队列
     *
     * @return
     */
    @Bean(QUEUE_NORMAL_WX)
    public Queue QUEUE_NORMAL_WX(){
        Map<String, Object> map = new HashMap<>();
        //队列设置5秒过期
        map.put("x-message-ttl", 5 * 1000);

        // 绑定该队列到死信交换机
        map.put("x-dead-letter-exchange", EXCHANGE_DEAD_PAYMENT);
        map.put("x-dead-letter-routing-key", ROUTING_KEY_DEAD_WX);

        return new Queue(QUEUE_NORMAL_WX, true, false, false, map);
    }

    /**
     * QUEUE_NORMAL_WX队列绑定交换机
     *
     * @param queue
     * @param exchange
     * @return
     */
    @Bean
    public Binding BINDING_QUEUE_NORMAL_PAYMENT(@Qualifier(QUEUE_NORMAL_WX) Queue queue,
                                             @Qualifier(EXCHANGE_NORMAL_PAYMENT) Exchange exchange){
        return BindingBuilder.bind(queue).to(exchange).with(ROUTING_KEY_PAYMENT_WX).noargs();
    }


    /** 死信配置 */

    public static final String QUEUE_DEAD_WX = "queue_dead_wx";
    public static final String EXCHANGE_DEAD_PAYMENT = "exchange_dead_payment";
    public static final String ROUTING_KEY_DEAD_WX = "dead.payment.wx";

    /**
     * 声明交换机
     *
     * @return
     */
    @Bean(EXCHANGE_DEAD_PAYMENT)
    public Exchange EXCHANGE_DEAD_PAYMENT(){
        //durable(true) 持久化，mq重启之后交换机还在
        return ExchangeBuilder.directExchange(EXCHANGE_DEAD_PAYMENT).durable(true).build();
    }

    /**
     * 声明QUEUE_DEAD_WX队列
     *
     * @return
     */
    @Bean(QUEUE_DEAD_WX)
    public Queue QUEUE_DEAD_WX(){

        return new Queue(QUEUE_DEAD_WX, true, false, false);
    }

    /**
     * QUEUE_DEAD_WX队列绑定交换机
     *
     * @param queue
     * @param exchange
     * @return
     */
    @Bean
    public Binding BINDING_QUEUE_DEAD_PAYMENT_WX(@Qualifier(QUEUE_DEAD_WX) Queue queue,
                                           @Qualifier(EXCHANGE_DEAD_PAYMENT) Exchange exchange){
        return BindingBuilder.bind(queue).to(exchange).with(ROUTING_KEY_DEAD_WX).noargs();
    }

}
