package org.liurb.springboot.advance.demo.class1.vo;

import lombok.Data;

/**
 * 学生分页展示vo
 *
 *
 * @Author Liurb
 * @Date 2022/11/13
 */
@Data
public class StudentPageVo {

    private Integer id;

    private String name;

    private Integer age;

    private String gender;

}
