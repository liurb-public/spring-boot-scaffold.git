package org.liurb.springboot.advance.demo.class5.dto;

import com.alibaba.fastjson2.annotation.JSONField;
import lombok.Data;

/**
 * @Author LiuRuiBin
 * @Date 2023/8/10
 */
@Data
public class FastJsonResponse {

    @JSONField(name = "is_get_award")
    private Boolean isGetAward;

}
