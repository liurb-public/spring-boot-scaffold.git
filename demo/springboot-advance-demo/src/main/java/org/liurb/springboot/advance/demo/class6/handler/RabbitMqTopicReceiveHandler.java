package org.liurb.springboot.advance.demo.class6.handler;

import com.rabbitmq.client.Channel;
import org.liurb.springboot.advance.demo.class6.conf.RabbitmqTopicConfig;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

/**
 * topic 模式消费者
 *
 * @Author LiuRuiBin
 * @Date 2023/8/10
 */
@ConditionalOnExpression("${mq.starter.config.enabled:true} && '${mq.starter.config.messaging}'.equals('rabbitmq_topic')")
@Component
public class RabbitMqTopicReceiveHandler {

    /**
     * 监听email队列
     *
     * @param msg
     * @param message
     * @param channel
     */
    @RabbitListener(queues = {RabbitmqTopicConfig.QUEUE_INFORM_EMAIL})
    public void receive_email(Object msg, Message message, Channel channel){
        System.out.println("QUEUE_INFORM_EMAIL msg："+msg);
        try {
            TimeUnit.SECONDS.sleep(5);//5秒
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    //监听sms队列
    @RabbitListener(queues = {RabbitmqTopicConfig.QUEUE_INFORM_SMS})
    public void receive_sms(Object msg, Message message, Channel channel){
        System.out.println("QUEUE_INFORM_SMS msg："+msg);
        try {
            TimeUnit.SECONDS.sleep(5);//5秒
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

}
