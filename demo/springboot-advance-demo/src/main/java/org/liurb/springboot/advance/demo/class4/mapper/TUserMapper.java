package org.liurb.springboot.advance.demo.class4.mapper;

import org.liurb.springboot.advance.demo.class4.entity.TUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Liurb
 * @since 2023-03-04
 */
public interface TUserMapper extends BaseMapper<TUser> {

}
