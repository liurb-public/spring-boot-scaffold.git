package org.liurb.springboot.advance.demo.class6.conf;

import org.springframework.amqp.core.*;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.context.annotation.Bean;

/**
 * rabbitmq fanout模式配置类
 *
 * @Author LiuRuiBin
 * @Date 2023/8/12
 */
@ConditionalOnExpression("${mq.starter.config.enabled:true} && '${mq.starter.config.messaging}'.equals('rabbitmq_fanout')")
@AutoConfiguration
public class RabbitmqFanoutConfig {

    public static final String QUEUE_NOTICE_PC = "queue_notice_pc";
    public static final String QUEUE_NOTICE_APP = "queue_notice_app";
    public static final String EXCHANGE_FANOUT_NOTICE="exchange_fanout_notice";

    /**
     * 声明交换机
     *
     * @return
     */
    @Bean(EXCHANGE_FANOUT_NOTICE)
    public Exchange EXCHANGE_FANOUT_NOTICE(){
        //durable(true) 持久化，mq重启之后交换机还在
        return ExchangeBuilder.fanoutExchange(EXCHANGE_FANOUT_NOTICE).durable(true).build();
    }

    /**
     * 声明QUEUE_NOTICE_PC队列
     *
     * @return
     */
    @Bean(QUEUE_NOTICE_PC)
    public Queue QUEUE_NOTICE_PC(){
        return new Queue(QUEUE_NOTICE_PC);
    }


    /**
     * 声明QUEUE_NOTICE_APP队列
     *
     * @return
     */
    @Bean(QUEUE_NOTICE_APP)
    public Queue QUEUE_NOTICE_APP(){
        return new Queue(QUEUE_NOTICE_APP);
    }

    /**
     * QUEUE_NOTICE_PC队列绑定交换机
     *
     * @param queue
     * @param exchange
     * @return
     */
    @Bean
    public Binding BINDING_QUEUE_NOTICE_PC(@Qualifier(QUEUE_NOTICE_PC) Queue queue,
                                              @Qualifier(EXCHANGE_FANOUT_NOTICE) Exchange exchange){
        return BindingBuilder.bind(queue).to(exchange).with("").noargs();
    }

    /**
     * QUEUE_NOTICE_APP队列绑定交换机
     *
     * @param queue
     * @param exchange
     * @return
     */
    @Bean
    public Binding BINDING_QUEUE_NOTICE_APP(@Qualifier(QUEUE_NOTICE_APP) Queue queue,
                                              @Qualifier(EXCHANGE_FANOUT_NOTICE) Exchange exchange){
        return BindingBuilder.bind(queue).to(exchange).with("").noargs();
    }

}
