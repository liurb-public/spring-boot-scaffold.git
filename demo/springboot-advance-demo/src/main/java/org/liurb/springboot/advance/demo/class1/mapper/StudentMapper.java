package org.liurb.springboot.advance.demo.class1.mapper;

import org.liurb.springboot.advance.demo.class1.entity.Student;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 学生表 Mapper 接口
 * </p>
 *
 * @author Liurb
 * @since 2022-11-13
 */
public interface StudentMapper extends BaseMapper<Student> {

}
