package org.liurb.springboot.advance.demo.class2.handler.payment.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.liurb.springboot.advance.demo.class2.handler.payment.annotation.Payment;
import org.liurb.springboot.advance.demo.class2.handler.payment.dto.PayOrder;
import org.liurb.springboot.advance.demo.class2.handler.payment.dto.PayResult;
import org.liurb.springboot.advance.demo.class2.handler.payment.service.PaymentHandleService;
import org.springframework.stereotype.Service;

/**
 * 银行卡支付处理
 *
 * @Author Liurb
 * @Date 2022/11/26
 */
@Slf4j
@Payment("card")
@Service
public class CardPaymentHandleServiceImpl implements PaymentHandleService {

    @Override
    public PayResult pay(PayOrder payOrder) {

        PayResult result = new PayResult();
        result.setOrder("card_202211261234567890");
        result.setCode(1);

        log.info("银行卡支付处理 订单信息:{} 支付结果:{}", payOrder, result);

        return result;
    }
}
