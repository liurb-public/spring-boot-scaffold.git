package org.liurb.springboot.advance.demo.class4.service;

import org.liurb.springboot.advance.demo.class4.entity.TUser;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Liurb
 * @since 2023-03-04
 */
public interface TUserService extends IService<TUser> {

}
