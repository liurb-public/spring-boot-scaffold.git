package org.liurb.springboot.advance.demo.class4.service.impl;

import org.liurb.springboot.advance.demo.class4.entity.TUser;
import org.liurb.springboot.advance.demo.class4.mapper.TUserMapper;
import org.liurb.springboot.advance.demo.class4.service.TUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Liurb
 * @since 2023-03-04
 */
@Service
public class TUserServiceImpl extends ServiceImpl<TUserMapper, TUser> implements TUserService {

}
