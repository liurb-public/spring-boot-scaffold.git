package org.liurb.springboot.advance.demo.class2.handler.payment.service;

import org.liurb.springboot.advance.demo.class2.handler.payment.dto.PayOrder;
import org.liurb.springboot.advance.demo.class2.handler.payment.dto.PayResult;

/**
 * 支付处理服务统一入口
 *
 * @Author Liurb
 * @Date 2022/11/26
 */
public interface VendorPaymentService {

    /**
     * 付款
     *
     * @param payOrder
     * @return
     */
    PayResult pay(PayOrder payOrder);

}
