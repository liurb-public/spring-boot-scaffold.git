package org.liurb.springboot.advance.demo.class5.controller;

import org.liurb.springboot.advance.demo.class5.dto.FastJsonRequest;
import org.liurb.springboot.advance.demo.class5.dto.FastJsonResponse;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * fastjson
 *
 * @Author LiuRuiBin
 * @Date 2023/8/10
 */
@RestController
@RequestMapping("/advance_demo_api/fast_json")
public class FastJsonController {


    @PostMapping("/test")
    public FastJsonResponse test(@RequestBody FastJsonRequest request) {
        FastJsonResponse response = new FastJsonResponse();

        Boolean isSelectedOption = request.getIsSelectedOption();

        response.setIsGetAward(isSelectedOption);
        return response;
    }

}
