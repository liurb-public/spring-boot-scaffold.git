package org.liurb.springboot.advance.demo.class3.annotation;

import java.lang.annotation.*;

/**
 * Redis缓存注解
 *
 * @Author Liurb
 * @Date 2022/12/3
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface MyRedisCache {

    /**
     * 缓存的key
     * 支持el表达式
     */
    String key() default "";

    /**
     * 默认失效时间为1天，单位为秒
     */
    long expire() default 86400;

    /**
     * 对返回数据进行缓存的判断依据
     * 支持el表达式
     * 形式如"#result.code==0"
     */
    String successFiled() default "";

}
