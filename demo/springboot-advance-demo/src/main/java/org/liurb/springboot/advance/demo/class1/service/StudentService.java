package org.liurb.springboot.advance.demo.class1.service;

import org.liurb.springboot.advance.demo.class1.entity.Student;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 学生表 服务类
 * </p>
 *
 * @author Liurb
 * @since 2022-11-13
 */
public interface StudentService extends IService<Student> {

}
