
## ShardingSphere-JDBC 结合 SpringBoot 和 Mybatis-Plus

class4 主要介绍 `ShardingSphere-JDBC` 的使用，实现简单的单库分表功能。

### 使用
1. 导入 `demo.sql`

### 说明
* 实例中分了三个表，分别为 t_user0、t_user1 和 t_user2

### 单元测试
1. 根据age字段实现分表，调用单元测试 `Class4InLineAgeDemoApplicationTests`
2. 根据phone字段实现分表，调用单元测试 `Class4ModPhoneDemoApplicationTests`