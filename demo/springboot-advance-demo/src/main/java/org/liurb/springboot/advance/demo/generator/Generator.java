package org.liurb.springboot.advance.demo.generator;

import org.liurb.springboot.starter.mysql.mybatis.generate.CodeGenerator;

/**
 * 代码生成器 main
 *
 * @Author Liurb
 * @Date 2022/11/28
 */
public class Generator {

    private static String jdbcUrl = "jdbc:mysql://127.0.0.1:3306/db?serverTimezone=UTC&useUnicode=true&characterEncoding=utf-8&useSSL=true&serverTimezone=Asia/Shanghai";
    private static String jdbcUserName = "root";

    private static String jdbcPassword = "123456";

    private static String daoOutputDirRelate = "/demo/springboot-advance-demo/src/main/java";

    private static String mapperOutputDirRelate = "/demo/springboot-advance-demo/src/main/resources/mapper";

    private static String author = "Liurb";

    private static String packageName = "org.liurb.springboot.advance.demo";

    private static String moduleName = "class1";

    private static String tableNames = "demo_student";

    private static String tablePrefix = "demo_";


    public static void main(String[] args) {
        CodeGenerator codeGenerator = CodeGenerator.builder().jdbcUrl(jdbcUrl).jdbcUserName(jdbcUserName).jdbcPassword(jdbcPassword)
                .daoOutputDirRelate(daoOutputDirRelate).mapperOutputDirRelate(mapperOutputDirRelate).author(author)
                .packageName(packageName).moduleName(moduleName).tableNames(tableNames).tablePrefix(tablePrefix)
                .build();

        codeGenerator.generator();
    }

}
