## RabbitMQ

### 使用注意事项
需要开启 RabbitMQ 服务，并在 `application.yml` 配置文件中，加入相关配置。同时，需要设置 `mq.starter.config.enabled` 值为 `true`

### 使用介绍

#### TOPIC模式

``` javascript
调整 `application.yml` 配置文件 `mq.starter.config.messaging` 值为 `rabbitmq_topic`
```
模拟sms和email两种渠道队列都绑定了交换机，现在有sms和email消息需要分别发送出去，对应的用户都能收到。

##### 配置
`RabbitmqTopicConfig` 配置了两个队列，一个交换机，使用 `topic` 模式

##### 生产者
在 `test` 测试用例 `class6` 内的 `ProducerTopicsSpringbootApplicationTests`

##### 消费者
`RabbitMqTopicReceiveHandler` 分别监听两个队列

#### FANOUT模式

``` javascript
调整 `application.yml` 配置文件 `mq.starter.config.messaging` 值为 `rabbitmq_fanout`
```
模拟PC端和APP端队列都绑定了广播交换机，现在广播一个消息需要两个端的用户都能收到。

##### 配置
`RabbitmqFanoutConfig` 配置了两个队列，一个交换机，使用 `fanout` 模式

##### 生产者
在 `test` 测试用例 `class6` 内的 `ProducerFanoutSpringbootApplicationTests`

##### 消费者
`RabbitMqFanoutReceiveHandler` 分别监听两个队列

#### DIRECT模式

``` javascript
调整 `application.yml` 配置文件 `mq.starter.config.messaging` 值为 `rabbitmq_direct`
```
加入了优先级队列，发送消息时可通过设置 `priority` 属性调整优先级

##### 配置
`RabbitmqDirectConfig` 配置了一个队列，一个交换机，使用 `direct` 模式

##### 生产者
在 `test` 测试用例 `class6` 内的 `ProducerDirectSpringbootApplicationTests`

##### 消费者
`RabbitMqDirectReceiveHandler` 监听一个队列

#### 死信队列

``` javascript
调整 `application.yml` 配置文件 `mq.starter.config.messaging` 值为 `rabbitmq_dead`
```
为正常队列设置超时时间，超时后会进入对应的死信队列内

##### 配置
`RabbitmqDirectConfig` 配置了一个队列，一个交换机，使用 `direct` 模式

正常队列中设置了5秒未消费就超时，并绑定死信队列和交换机
```java
        Map<String, Object> map = new HashMap<>();
        //队列设置5秒过期
        map.put("x-message-ttl", 5 * 1000);

        // 绑定该队列到死信交换机
        map.put("x-dead-letter-exchange", EXCHANGE_DEAD_PAYMENT);
        map.put("x-dead-letter-routing-key", ROUTING_KEY_DEAD_WX);
```

##### 生产者
在 `test` 测试用例 `class6` 内的 `ProducerDeadSpringbootApplicationTests`

##### 消费者
`RabbitMqDeadReceiveHandler` 监听正常队列和死信队列