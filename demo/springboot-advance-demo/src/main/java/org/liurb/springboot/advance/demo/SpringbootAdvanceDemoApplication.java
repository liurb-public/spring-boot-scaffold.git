package org.liurb.springboot.advance.demo;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.context.annotation.ComponentScan;

@ServletComponentScan
@ComponentScan("org.liurb")
@MapperScan("org.liurb.**.mapper")
@SpringBootApplication
public class SpringbootAdvanceDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootAdvanceDemoApplication.class, args);
    }

}
