CREATE TABLE `t_user_0` (
                            `id` int NOT NULL AUTO_INCREMENT,
                            `name` varchar(64) DEFAULT NULL,
                            `age` int DEFAULT '0',
                            `phone` varchar(64) DEFAULT NULL,
                            PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `t_user_1` (
                            `id` int NOT NULL AUTO_INCREMENT,
                            `name` varchar(64) DEFAULT NULL,
                            `age` int DEFAULT '0',
                            `phone` varchar(64) DEFAULT NULL,
                            PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `t_user_2` (
                            `id` int NOT NULL AUTO_INCREMENT,
                            `name` varchar(64) DEFAULT NULL,
                            `age` int DEFAULT '0',
                            `phone` varchar(64) DEFAULT NULL,
                            PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;