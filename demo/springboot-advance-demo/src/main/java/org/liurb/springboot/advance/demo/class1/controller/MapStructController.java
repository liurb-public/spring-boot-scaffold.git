package org.liurb.springboot.advance.demo.class1.controller;

import jakarta.annotation.Resource;
import org.liurb.springboot.advance.demo.class1.entity.Student;
import org.liurb.springboot.advance.demo.class1.mapstruct.StudentStructMapper;
import org.liurb.springboot.advance.demo.class1.service.StudentService;
import org.liurb.springboot.advance.demo.class1.vo.StudentHomeVo;
import org.liurb.springboot.advance.demo.class1.vo.StudentPageVo;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import java.util.List;
import java.util.stream.Collectors;

/**
 * mapstruct实例控制器
 *
 * @Author Liurb
 * @Date 2022/11/13
 */
@RestController
@RequestMapping("/advance_demo_api/mapstruct")
public class MapStructController {

    @Resource
    StudentService studentService;

    @GetMapping("/home/{id}")
    public StudentHomeVo home(@PathVariable("id")Integer id) {

        Student student = studentService.getById(id);

        StudentHomeVo studentHomeVo = StudentStructMapper.INSTANCES.toStudentHomeVo(student);

        return studentHomeVo;
    }

    @GetMapping("/page")
    public List<StudentPageVo> page() {
        List<Student> students = studentService.list();

        List<StudentPageVo> studentPageVos = students.stream().map(item -> {

            StudentPageVo studentPageVo = StudentStructMapper.INSTANCES.toStudentPageVo(item);
            return studentPageVo;
        }).collect(Collectors.toList());

        return studentPageVos;
    }

}
