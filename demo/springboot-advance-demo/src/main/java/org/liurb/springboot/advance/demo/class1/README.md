
## 优雅使用 MapStruct 进行类复制

class1 主要介绍 `MapStruct` 的使用，使繁琐的类字段复制变得更加高效和简单。

### MAPPER
定义实体转换接口 `StudentStructMapper`，将实体 `Student` 转换为前端所需字段 `StudentHomeVo` 和 `StudentPageVo`

### 使用
1. 导入 `demo_student.sql`
2. 调用控制器 `controller` 的接口测试