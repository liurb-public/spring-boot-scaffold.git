package org.liurb.springboot.advance.demo.class7.repository;

import org.liurb.springboot.advance.demo.class7.bean.EsBook;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 书本信息 Repository接口
 *
 * 可以完成一些简单的操作，比如插入更新，全亮查询等
 *
 * @Author LiuRuiBin
 * @Date 2023/10/22
 */
@Repository
public interface EsBookRepository extends ElasticsearchRepository<EsBook, Integer> {

    List<EsBook> findByTitle(String title);

    List<EsBook> findByIntro(String intro);
}
