package org.liurb.springboot.advance.demo.class6.conf;

import org.springframework.amqp.core.*;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.context.annotation.Bean;

import java.util.HashMap;
import java.util.Map;

/**
 * rabbitmq direct模式配置类
 *
 * @Author LiuRuiBin
 * @Date 2023/8/14
 */
@ConditionalOnExpression("${mq.starter.config.enabled:true} && '${mq.starter.config.messaging}'.equals('rabbitmq_direct')")
@AutoConfiguration
public class RabbitmqDirectConfig {

    public static final String QUEUE_DIRECT_CARD = "queue_direct_card";
    public static final String EXCHANGE_DIRECT_PAYMENT = "exchange_direct_payment";
    public static final String ROUTING_KEY_CARD="payment.card";

    /**
     * 声明交换机
     *
     * @return
     */
    @Bean(EXCHANGE_DIRECT_PAYMENT)
    public Exchange EXCHANGE_FANOUT_NOTICE(){
        //durable(true) 持久化，mq重启之后交换机还在
        return ExchangeBuilder.directExchange(EXCHANGE_DIRECT_PAYMENT).durable(true).build();
    }

    /**
     * 声明QUEUE_DIRECT_CARD队列
     *
     * @return
     */
    @Bean(QUEUE_DIRECT_CARD)
    public Queue QUEUE_DIRECT_CARD(){
        Map<String, Object> map = new HashMap<>();
        //一般设置10个优先级，数字越大，优先级越高
        map.put("x-max-priority", 10);

        return new Queue(QUEUE_DIRECT_CARD, true, false, false, map);
    }

    /**
     * QUEUE_DIRECT_CARD队列绑定交换机
     *
     * @param queue
     * @param exchange
     * @return
     */
    @Bean
    public Binding BINDING_QUEUE_DIRECT_CARD(@Qualifier(QUEUE_DIRECT_CARD) Queue queue,
                                           @Qualifier(EXCHANGE_DIRECT_PAYMENT) Exchange exchange){
        return BindingBuilder.bind(queue).to(exchange).with(ROUTING_KEY_CARD).noargs();
    }

}
