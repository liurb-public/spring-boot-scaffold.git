## Elasticsearch8.x
elasticsearch版本为8.10.2

### IK分词器
IK分词器插件地址  
https://github.com/medcl/elasticsearch-analysis-ik  

官网的版本可能没有最新的，有小伙伴自己构建了其他高版本的  
https://github.com/lizongbo/elasticsearch-analysis-ik/releases/download/v8.10.2/elasticsearch-analysis-ik-8.10.2.zip

### 使用注意事项
需要开启 Elasticsearch 服务，并在 `application.yml` 配置文件中，加入相关配置。同时，需要设置 `es.starter.config.enabled` 值为 `true`

### 测试
在 `test` 测试用例 `class7` 内的 `Class7ElasticSearchApplicationTests`
