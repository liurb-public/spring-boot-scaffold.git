## spring-boot-scaffold

### 介绍
`springboot`开发手脚架，意在构建快速开发模块，主要分为 `parent` 父项目和 `starter` 模块：
* `parent` 父项目，统一 `springboot` 项目中的软件包版本，分为 公共`backend-parent` 和 针对spring-cloud`backend-cloud-parent`
* `starter` 项目，按照组件分为不同的模块，模仿 `spring-boot-starter-xxx`

### 环境
项目使用 jdk21 和 springboot3.0

### 使用
本地使用方式，打包并安装以下项目 ` mvn package / mvn install ` :
1. `parent` 项目
2. `starter` 项目
3. 新建的 `springboot` 项目分为引入所需 `starter` 模块，具体可见 `demo` 项目

### 已知问题
1. 更新解决 `mybatis-plus` 的 `3.5.2.7-SNAPSHOT` 版本适配 `springboot3.0`，启动 `demo` 项目时会报异常的问题
```html
Property 'sqlSessionFactory' or 'sqlSessionTemplate' are required
```