package org.liurb.springboot.starter.elasticsearch.conf;

import co.elastic.clients.elasticsearch.ElasticsearchClient;
import co.elastic.clients.json.jackson.JacksonJsonpMapper;
import co.elastic.clients.transport.ElasticsearchTransport;
import co.elastic.clients.transport.rest_client.RestClientTransport;
import org.apache.http.HttpHost;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.elasticsearch.client.RestClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.context.annotation.Bean;
import org.springframework.data.elasticsearch.config.ElasticsearchConfigurationSupport;

/**
 * es配置类
 *
 * @Author LiuRuiBin
 * @Date 2023/10/15
 */
@ConditionalOnExpression("${es.starter.config.enabled:true}")
@AutoConfiguration
public class ElasticSearchConfig extends ElasticsearchConfigurationSupport {

    @Value("${es.starter.config.hostname:127.0.0.1}")
    private String hostname;

    @Value("${es.starter.config.port:9200}")
    private Integer port;

    @Value("${es.starter.config.scheme:http}")
    private String scheme;

    @Value("${es.starter.config.userName}")
    private String userName;

    @Value("${es.starter.config.password}")
    private String password;

    @Bean
    public ElasticsearchClient elasticsearchClient(){
        CredentialsProvider credentialsProvider = new BasicCredentialsProvider();
        // 设置账号密码.
        credentialsProvider.setCredentials(AuthScope.ANY, new UsernamePasswordCredentials(userName, password));

        RestClient restClient = RestClient.builder(new HttpHost(hostname, port,scheme))
                .setHttpClientConfigCallback(httpClientBuilder ->
                        httpClientBuilder.setDefaultCredentialsProvider(credentialsProvider)).build();

        ElasticsearchTransport transport = new RestClientTransport(restClient, new JacksonJsonpMapper());
        return new ElasticsearchClient(transport);
    }

}
