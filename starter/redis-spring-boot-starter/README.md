# redis-spring-boot-starter

## 介绍
* 默认集成使用 `fastjson` 作为序列化解析器

## 打包
可先本地进行打包调试，后续可以上传到私服中
```shell
mvn package
mvn install
```

## 使用
本地打包后，在所需项目 `pom.xml` 引入此项目
```xml
        <dependency>
            <groupId>org.liurb.springboot.scaffold</groupId>
            <artifactId>redis-spring-boot-starter</artifactId>
            <version>${redis-spring-boot-starter.version}</version>
        </dependency>
```
在 `yml` 文件中加入 `redis` 链接配置等信息，并开启使用 `fastjson` 作为默认的序列化解析器
```yaml
#redis starter 配置样例
redis:
  starter:
    config:
      #序列化
      serializer: fastjson
      host: 127.0.0.1
      port: 6379
      database: 8
      password:
      lettuce:
        pool:
          max-active: 8
          max-idle: 8
          min-idle: 0
          max-wait: -1
```

## 使用其他序列化解析器

### gson
项目已集成 `gson` 配置，只要在序列化器指定为 `gson` 即可
```yaml
#redis starter 配置样例
redis:
  starter:
    config:
      #序列化
      serializer: gson
      host: 127.0.0.1
      port: 6379
      database: 8
      password:
      lettuce:
        pool:
          max-active: 8
          max-idle: 8
          min-idle: 0
          max-wait: -1
```


## 高级应用

### 多 `redis` 库配置

创建一个新的 `RedisConfig`类，并继承BackendFastjsonRedisConfig。

```java
//创建新的 Fastjson2RedisConfig 类
@AutoConfiguration
@EnableConfigurationProperties(value = RedisConfig2Properties.class)
@ConditionalOnProperty(
        name = "redis.starter.config2.serializer",
        havingValue = "fastjson"
)
public class Fastjson2RedisConfig extends BackendFastjsonRedisConfig {

    @Value("${redis.starter.config2.database:1}")
    private int database;

    @Bean(name = "redisTemplate2")
    public RedisTemplate<String, Object> redisTemplate() {

        return getRedisTemplate(database);
    }

}
```

同时创建一个新的 `RedisConfigProperties`类，用于配置redis连接信息。
```java
//创建新的 RedisConfig2Properties类
@Data
@Component
@ConfigurationProperties(prefix = "redis.starter.config2")
public class RedisConfig2Properties {

    private String serializer;

    private String host;

    private int port;

    private String password;

    private LettuceConfig lettuce;

    @Data
    public static class LettuceConfig {

        private PoolConfig pool;

        @Data
        public static class PoolConfig {

            private int maxActive;

            private int maxIdle;

            private int minIdle;

            private int maxWait;

        }

    }

}
```

使用时，注入 `redisTemplate2` 实例
```java
@Resource(name = "redisTemplate2")
RedisTemplate redisTemplate;
```