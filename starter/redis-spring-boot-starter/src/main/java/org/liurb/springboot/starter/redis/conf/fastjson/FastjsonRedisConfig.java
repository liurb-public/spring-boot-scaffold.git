package org.liurb.springboot.starter.redis.conf.fastjson;

import org.liurb.springboot.starter.redis.conf.base.RedisConfigProperties;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.data.redis.core.RedisTemplate;


/**
 * redis配置类
 *
 * @Author Liurb
 * @Date 2022/11/28
 */
@AutoConfiguration
@EnableConfigurationProperties(value = RedisConfigProperties.class)
@ConditionalOnProperty(
        name = "redis.starter.config.serializer",
        havingValue = "fastjson"
)
public class FastjsonRedisConfig extends BackendFastjsonRedisConfig {

    @Value("${redis.starter.config.database:1}")
    private int database;

    @Bean(name = "redisTemplate")
    public RedisTemplate<String, Object> redisTemplate() {

        return getRedisTemplate(database);
    }

}
