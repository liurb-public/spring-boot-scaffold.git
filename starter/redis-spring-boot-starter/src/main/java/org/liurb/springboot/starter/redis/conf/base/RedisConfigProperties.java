package org.liurb.springboot.starter.redis.conf.base;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * redis配置信息
 *
 * @Author Liurb
 * @Date 2022/11/28
 */
@Data
@Component
@ConfigurationProperties(prefix = "redis.starter.config")
public class RedisConfigProperties {

    private String serializer;

    private String host;

    private int port;

    private String password;

    private LettuceConfig lettuce;

    @Data
    public static class LettuceConfig {

        private PoolConfig pool;

        @Data
        public static class PoolConfig {

            private int maxActive;

            private int maxIdle;

            private int minIdle;

            private int maxWait;

        }

    }

}
