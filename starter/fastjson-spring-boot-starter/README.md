# fastjson-spring-boot-starter

## 介绍
集成 `fastjson` 配置，并替换 `springboot` 的默认消息解析器 `jackson`

## 打包
可先本地进行打包调试，后续可以上传到私服中
```shell
mvn package
mvn install
```

## 使用
本地打包后，在所需项目 `pom.xml` 引入此项目
```xml
        <dependency>
            <groupId>org.liurb.springboot.scaffold</groupId>
            <artifactId>fastjson-spring-boot-starter</artifactId>
            <version>${fastjson-spring-boot-starter.version}</version>
        </dependency>
```

开启配置，在 `yml` 文件中加入以下配置
```yaml
#web starter 配置样例
web:
  starter:
    #消息解析器配置
    http-message:
      config:
        #使用fastjson
        converter: fastjson
```

## 高级应用

### `FastJsonParser` 注解
添加在请求参数中，可自动适配注入请求参数到实体，兼容GET/POST方法和多种数据格式。
```java
//在controller控制器中的方法

@GetMapping("/test")
public String test(@FastJsonParser DemoTestRequest demoTestRequest) {
    //todo...

    return "ok";
}
```