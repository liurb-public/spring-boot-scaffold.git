package org.liurb.springboot.starter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FastjsonSpringBootStarterApplication {

    public static void main(String[] args) {
        SpringApplication.run(FastjsonSpringBootStarterApplication.class, args);
    }

}
