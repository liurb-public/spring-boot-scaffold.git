package org.liurb.springboot.starter.fastjson.utils;

import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONObject;

import java.util.Map;

/**
 * fastjson帮助类
 *
 * @Author Liurb
 * @Date 2022/11/28
 */
public class FastjsonUtil {

    /**
     * string类型map转jsonObject
     *
     * @param stringMap
     * @return
     */
    public static JSONObject stringMapToJson(Map<String, String> stringMap) {

        return JSONObject.parseObject(JSON.toJSONString(stringMap));
    }

    /**
     * obj类型map转jsonObject
     *
     * @param objMap
     * @return
     */
    public static JSONObject objectMapToJson(Map<String, Object> objMap) {

        JSONObject jsonObject = new JSONObject();

        for (String key : objMap.keySet()) {
            jsonObject.put(key, objMap.get(key));
        }

        return jsonObject;
    }

}
