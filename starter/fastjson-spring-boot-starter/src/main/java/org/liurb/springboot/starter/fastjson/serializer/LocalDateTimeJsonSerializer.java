package org.liurb.springboot.starter.fastjson.serializer;

import com.alibaba.fastjson2.JSONWriter;
import com.alibaba.fastjson2.writer.ObjectWriter;

import java.lang.reflect.Type;
import java.time.LocalDateTime;
import java.time.ZoneOffset;

/**
 * fastjson日期序列化类
 *
 * 序列化
 *
 * 统一将LocalDateTime转为毫秒数
 *
 * @Author Liurb
 * @Date 2022/11/28
 */
public class LocalDateTimeJsonSerializer implements ObjectWriter<String> {

    @Override
    public void write(JSONWriter jsonWriter, Object object, Object fieldName, Type fieldType, long features) {

        if (object == null) {
            jsonWriter.writeNull();
            return;
        }

        if (object instanceof LocalDateTime) {
            LocalDateTime localDateTime = (LocalDateTime) object;
            jsonWriter.writeString(localDateTime.toInstant(ZoneOffset.of("+8")).toEpochMilli() + "");
        }
    }

}

