# common-spring-boot-starter

## 介绍
提供一些工具类、公共注解、校验等

## 打包
可先本地进行打包调试，后续可以上传到私服中
```shell
mvn package
mvn install
```

## 使用
本地打包后，在所需项目 `pom.xml` 引入此项目
```xml
        <dependency>
            <groupId>org.liurb.springboot.scaffold</groupId>
            <artifactId>common-spring-boot-starter</artifactId>
            <version>${common-spring-boot-starter.version}</version>
        </dependency>
```