package org.liurb.springboot.starter.common.annotation;


import jakarta.validation.Constraint;
import jakarta.validation.Payload;
import org.liurb.springboot.starter.common.validator.DateTimeValidator;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 日期时间校验注解
 *
 * 用于yyyy-MM-dd格式的校验
 *
 * @Author Liurb
 * @Date 2022/11/28
 */
@Target({ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = DateTimeValidator.class)
public @interface DateTimeAnnotation {

    String message() default "格式错误";

    String format() default "yyyy-MM-dd";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

}
