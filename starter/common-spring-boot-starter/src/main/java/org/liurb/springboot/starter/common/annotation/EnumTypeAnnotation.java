package org.liurb.springboot.starter.common.annotation;


import jakarta.validation.Constraint;
import jakarta.validation.Payload;
import org.liurb.springboot.starter.common.validator.EnumTypeValidator;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 枚举校验注解
 *
 * 用于对请求参数的类型字段进行校验，一般这些字段都有对应的枚举类，所以可以通过这个注解对其进行相应的校验
 *
 * @Author Liurb
 * @Date 2022/11/28
 */
@Target({ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = EnumTypeValidator.class)
public @interface EnumTypeAnnotation {

    String message() default "格式错误";

    Class<? extends Enum<?>> clazz();

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
