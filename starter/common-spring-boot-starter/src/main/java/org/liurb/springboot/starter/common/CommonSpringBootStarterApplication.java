package org.liurb.springboot.starter.common;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CommonSpringBootStarterApplication {

    public static void main(String[] args) {
        SpringApplication.run(CommonSpringBootStarterApplication.class, args);
    }

}
