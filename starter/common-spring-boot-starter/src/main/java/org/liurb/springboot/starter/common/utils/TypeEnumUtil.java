package org.liurb.springboot.starter.common.utils;

import cn.hutool.core.util.ReflectUtil;

/**
 * 类型枚举帮助类
 *
 * @Author Liurb
 * @Date 2022/11/28
 */
public class TypeEnumUtil {

    /**
     * 根据类型查询对应的名称
     *
     * @param clazz
     * @param type
     * @return
     */
   public static String getNameByType(Class<? extends Enum<?>> clazz, int type) {

       Enum<?>[] enums = (Enum[])clazz.getEnumConstants();
       for (Enum e : enums) {
           String enName = (String) ReflectUtil.getFieldValue(e, "name");
           Integer enType = (Integer) ReflectUtil.getFieldValue(e, "type");
           if (enType == type) {
               return enName;
           }
       }
       return null;
   }

    /**
     * 根据类型查询对应的值
     *
     * @param clazz
     * @param type
     * @param fieldName 键
     * @return
     */
   public static Object getFieldByType(Class<? extends Enum<?>> clazz, int type, String fieldName) {
       Enum<?>[] enums = (Enum[])clazz.getEnumConstants();
       for (Enum e : enums) {
           Object fieldValue = ReflectUtil.getFieldValue(e, fieldName);
           Integer enType = (Integer) ReflectUtil.getFieldValue(e, "type");
           if (enType == type) {
               return fieldValue;
           }
       }
       return null;
   }

    /**
     * 根据类型查询对应的枚举名称
     *
     * @param clazz
     * @param type
     * @return
     */
   public static String getEnumNameByType(Class<? extends Enum<?>> clazz, int type) {

       Enum<?>[] enums = (Enum[])clazz.getEnumConstants();
       for (Enum e : enums) {
           Integer enType = (Integer) ReflectUtil.getFieldValue(e, "type");
           if (enType == type) {
               return e.name();
           }
       }

       return null;
   }

    /**
     * 根据类型查询对应的枚举名称
     *
     * @param clazz
     * @param type
     * @return
     */
   public static String getEnumNameByType(Class<? extends Enum<?>> clazz, String type) {

        Enum<?>[] enums = (Enum[])clazz.getEnumConstants();
        for (Enum e : enums) {
            String enType = (String) ReflectUtil.getFieldValue(e, "type");
            if (enType.equals(type)) {
                return e.name();
            }
        }

        return null;
   }

    /**
     * 根据枚举名称查询对应的类型
     *
     * @param clazz
     * @param enumName
     * @return
     */
   public static Object getTypeByEnumName(Class<? extends Enum<?>> clazz, String enumName) {

        Enum<?>[] enums = (Enum[])clazz.getEnumConstants();
        for (Enum e : enums) {
            String enName = e.name();
            if (enName == enumName) {
                return ReflectUtil.getFieldValue(e, "type");
            }
        }

        return null;
    }

    /**
     * 根据枚举名称查询对应的字段值
     *
     * @param clazz
     * @param enumName
     * @param findFieldName
     * @return
     */
    public static Object getFieldByEnumName(Class<? extends Enum<?>> clazz, String enumName, String findFieldName) {

        Enum<?>[] enums = (Enum[])clazz.getEnumConstants();
        for (Enum e : enums) {
            String enName = e.name();
            if (enName == enumName) {
                return ReflectUtil.getFieldValue(e, findFieldName);
            }
        }

        return null;
    }

}
