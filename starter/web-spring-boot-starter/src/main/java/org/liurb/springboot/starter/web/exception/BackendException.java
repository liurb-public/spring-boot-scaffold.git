package org.liurb.springboot.starter.web.exception;

import lombok.Data;

/**
 * 自定义异常
 *
 * 异常默认不发送邮件
 *
 * @Author Liurb
 * @Date 2022/11/28
 */
@Data
public class BackendException extends RuntimeException {

    private Integer code;
    private String message;
    private boolean isSendMail;

    public BackendException(String message) {
        super(message);
        this.message = message;
    }

    public BackendException(Integer code, String message) {
        super(message);
        this.code = code;
        this.message = message;
    }

    public BackendException(Integer code, String message, boolean isSendMail) {
        super(message);
        this.code = code;
        this.message = message;
        this.isSendMail = isSendMail;
    }

}
