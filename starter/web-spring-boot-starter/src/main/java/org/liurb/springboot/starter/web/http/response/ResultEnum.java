package org.liurb.springboot.starter.web.http.response;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 统一返回信息枚举
 *
 * @Author Liurb
 * @Date 2022/11/28
 */
@Getter
@AllArgsConstructor
public enum ResultEnum {

    /**
     * 默认失败
     */
    DEFAULT_FAIL(-99, "失败"),
    /**
     * 接口调用错误返回
     *
     */
    API_ERROR(-2,"接口调用错误"),
    /**
     * 系统错误返回
     *
     */
    SYS_ERROR(-1,"系统错误"),
    /**
     * 成功返回
     */
    SUCCESS(0,"成功"),
    /**
     * 授权失败返回
     */
    AUTH_ERROR(1001,"授权失败"),
    /**
     * 参数验证失败返回
     */
    PARAMS_ERROR(1002,"参数验证失败"),
    /**
     * 过滤器抛出错误异常
     */
    FILTER_ERROR(1101,"过滤验证失败"),
    ;

    final Integer code;
    final String msg;
}
