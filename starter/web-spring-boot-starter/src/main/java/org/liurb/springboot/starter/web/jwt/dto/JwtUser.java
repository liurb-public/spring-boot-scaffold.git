package org.liurb.springboot.starter.web.jwt.dto;

import lombok.Data;

/**
 * jwt用户信息
 *
 * @Author Liurb
 * @Date 2022/11/28
 */
@Data
public class JwtUser {

    private String settingId;
    private String unionId;
    private String appId;
    private String openId;
    private String channel;
    private String nickname;
    private String headImgUrl;
    private Integer sex;
    private String country;
    private String province;
    private String city;
    private String phone;

}
