package org.liurb.springboot.starter.web.mail.conf;

import cn.hutool.extra.mail.MailAccount;
import lombok.Data;
import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;

/**
 * 邮件配置
 *
 * 需要子项目引入mail.setting配置文件
 *
 * @Author Liurb
 * @Date 2022/11/28
 */
@Data
@AutoConfiguration
@ConfigurationProperties("mail")
public class BackendMailConfig {

    private String host;

    private Integer port;

    private String from;

    private String user;

    private String pass;

    private Boolean sslEnable;

    @Bean
    public MailAccount getMailAccount() {
        MailAccount mailAccount = new MailAccount();
        mailAccount.setHost(host);
        mailAccount.setPort(port);
        mailAccount.setFrom(from);
        mailAccount.setUser(user);
        mailAccount.setPass(pass);
        mailAccount.setSslEnable(sslEnable);
        return mailAccount;
    }

}
