package org.liurb.springboot.starter.web.http.response;

/**
 * 统一结果返回帮助类
 *
 * @Author Liurb
 * @Date 2022/11/28
 */
public class ResultUtil {

    /**
     * 成功返回
     *
     * @param object
     * @return
     */
    public static Result success(Object object){
        Result result = new Result();
        result.setSuccess(true);
        result.setCode(ResultEnum.SUCCESS.getCode());
        result.setMsg(ResultEnum.SUCCESS.getMsg());
        result.setData(object);
        return result;
    }

    public static Result success(Integer code, String msg, Object object){
        Result result = new Result();
        result.setSuccess(true);
        result.setCode(code);
        result.setMsg(msg);
        result.setData(object);
        return result;
    }

    /**
     * 成功但不带数据
     *
     * @return
     */
    public static Result success(){
        return success(null);
    }

    /**
     * 默认失败返回
     *
     * @param msg
     * @return
     */
    public static Result fail(String msg) {
        Result result = new Result();
        result.setSuccess(false);
        result.setCode(ResultEnum.DEFAULT_FAIL.getCode());
        result.setMsg(msg);
        return result;
    }

    /**
     * 失败返回
     *
     * @param code
     * @param msg
     * @return
     */
    public static Result fail(Integer code, String msg){
        Result result = new Result();
        result.setSuccess(false);
        if (null == code) {
            code = ResultEnum.DEFAULT_FAIL.getCode();
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }


}
