package org.liurb.springboot.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WebSpringBootStarterApplication {

	public static void main(String[] args) {
		SpringApplication.run(WebSpringBootStarterApplication.class, args);
	}

}
