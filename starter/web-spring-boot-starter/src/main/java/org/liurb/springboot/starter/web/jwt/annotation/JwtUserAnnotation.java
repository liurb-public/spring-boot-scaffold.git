package org.liurb.springboot.starter.web.jwt.annotation;

import java.lang.annotation.*;

/**
 * jwt用户注解
 *
 * 配合aop切面JwtUserAspect实现
 *
 *
 * @Author Liurb
 * @Date 2022/11/28
 */
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface JwtUserAnnotation {

    boolean required() default true;

}
