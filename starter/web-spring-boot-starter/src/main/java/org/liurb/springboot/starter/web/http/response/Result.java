package org.liurb.springboot.starter.web.http.response;

import lombok.Data;

/**
 * 统一结果返回实体
 *
 * @Author Liurb
 * @Date 2022/11/28
 */
@Data
public class Result<T> {

    private Integer code;
    private String msg;
    private Boolean success;
    private T data;

}
