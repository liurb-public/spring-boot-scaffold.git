package org.liurb.springboot.starter.web.constants;

/**
 * 公共常量
 *
 * @Author Liurb
 * @Date 2022/11/28
 */
public class BackendPublicConstants {

    /**
     * 授权密钥
     */
    public final static String AUTH_SECRET = "org.liurb#2A$UPih)+jR6TVYi";
}
