# web-spring-boot-starter

## 介绍
集成 web 项目开发中常用配置内容：

* Rest 模板配置 `RestTemplate`，请求日志打印，连接池配置等
* 公共异常处理
* 公共接口请求返回日志打印
* jwt
* 邮件配置
* 解决输入流只能读取一次的问题
* 统一接口数据返回

## 使用
可以查看 `springboot-advance-demo` 的 `pom.xml` 配置