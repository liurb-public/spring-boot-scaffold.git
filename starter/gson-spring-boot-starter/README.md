# gson-spring-boot-starter

## 介绍
集成 `gson` 配置，并替换 `springboot` 的默认消息解析器 `jackson`

## 使用
具体可查看 `springboot-gson-demo` 项目