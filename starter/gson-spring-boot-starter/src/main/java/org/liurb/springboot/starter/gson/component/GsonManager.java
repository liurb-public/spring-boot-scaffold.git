package org.liurb.springboot.starter.gson.component;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import org.liurb.springboot.starter.gson.strategy.AnnotationExclusionStrategy;

import java.lang.reflect.Type;
import java.util.Map;

/**
 * gson管理器
 *
 * 提供一个默认配置的gson单例
 *
 * @Author Liurb
 * @Date 2022/11/30
 */
public class GsonManager {

    private static Gson gson;

    private static GsonManager gsonManager;

    private GsonManager() {
        //默认配置
        gson =  new GsonBuilder().serializeNulls().disableHtmlEscaping()
                .setExclusionStrategies(new AnnotationExclusionStrategy()).create();
    }

    public static GsonManager getInstance() {
        if (gsonManager == null) {
            synchronized (GsonManager.class) {
                if (gsonManager == null) {
                    gsonManager = new GsonManager();
                }
            }
        }
        return gsonManager;
    }

    /**
     * 获取gson对象
     *
     * @return
     */
    public Gson getGson() {
        return gson;
    }

    /**
     * json字符串转实体对象
     *
     * @param json
     * @param type
     * @return
     * @param <T>
     */
    private <T> T fromJson(String json, Type type) {
        return gson.fromJson(json, type);
    }

    /**
     * json字符串转实体对象
     *
     * @param json
     * @param clz
     * @return
     * @param <T>
     */
    public <T> T convert(String json, Class<T> clz) {
        return fromJson(json, clz);
    }

    /**
     * 对象转json字符串
     *
     * @param obj 对象
     * @return String
     */
    public String toJsonText(Object obj) {
        return gson.toJson(obj);
    }

    /**
     * 任意对象转json对象
     *
     * @param obj
     * @return
     */
    public JsonObject toJsonObject(Object obj) {
        String jsonText = this.toJsonText(obj);
        return this.toJsonObject(jsonText);
    }

    /**
     * json字符串转json对象
     *
     * @param json
     * @return
     */
    public JsonObject toJsonObject(String json) {

        return gson.fromJson(json, JsonObject.class);
    }

    /**
     * 字符串map转json对象
     *
     * @param stringMap
     * @return
     */
    public JsonObject stringMapToJson(Map<String, String> stringMap) {

        JsonObject jsonObject = new JsonObject();
        for (String key : stringMap.keySet()) {
            jsonObject.addProperty(key, stringMap.get(key));
        }

        return jsonObject;
    }

    /**
     * 实体对象转字符串map
     *
     * @param obj
     * @return
     */
    public Map<String, String> objToStringMap(Object obj) {
        String jsonText = gson.toJson(obj);

        Type mapType = new TypeToken<Map<String, String>>(){}.getType();

        return this.fromJson(jsonText, mapType);
    }

    /**
     * 实体对象转字符串map
     *
     * @param obj
     * @return
     */
    public Map<String, Object> objToObjectMap(Object obj) {
        String jsonText = gson.toJson(obj);

        Type mapType = new TypeToken<Map<String, Object>>(){}.getType();

        return this.fromJson(jsonText, mapType);
    }

}
