package org.liurb.springboot.starter.gson.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 配合gson排除字段序列化使用
 *
 * AnnotationExclusionStrategy策略
 *
 * @Author Liurb
 * @Date 2022/11/30
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface FieldExclude {

}
