package org.liurb.springboot.starter.gson.strategy;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import org.liurb.springboot.starter.gson.annotation.FieldExclude;


/**
 * gson排除序列化字段注解策略
 *
 * 配合FieldExclude注解使用
 *
 * @Author Liurb
 * @Date 2022/11/30
 */
public class AnnotationExclusionStrategy implements ExclusionStrategy {

    @Override
    public boolean shouldSkipField(FieldAttributes fieldAttributes) {

        return fieldAttributes.getAnnotation(FieldExclude.class) != null;
    }

    @Override
    public boolean shouldSkipClass(Class<?> aClass) {
        return false;
    }

}
