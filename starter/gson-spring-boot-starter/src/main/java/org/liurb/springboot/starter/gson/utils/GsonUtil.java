package org.liurb.springboot.starter.gson.utils;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

/**
 * gson帮助类
 *
 * 主要用作获取json对象的值
 *
 * 由于gson取值判空不能使用 null 判断，所以在取值的时候会比较麻烦，所以这里抽离出几种取值的情况
 *
 * @Author Liurb
 * @Date 2022/11/30
 */
public class GsonUtil {

    /**
     * 获取gson对象的string值
     *
     * @param jsonObject
     * @param key
     * @return
     */
    public static String getStringValue(JsonObject jsonObject, String key) {

        JsonElement jsonElement = jsonObject.get(key);
        if (jsonElement != null && !jsonElement.isJsonNull()) {

            return jsonElement.getAsString();
        }

        return null;
    }

    /**
     * 获取gson对象的int值
     *
     * @param jsonObject
     * @param key
     * @return
     */
    public static Integer getIntegerValue(JsonObject jsonObject, String key) {

        JsonElement jsonElement = jsonObject.get(key);
        if (jsonElement != null && !jsonElement.isJsonNull()) {

            return jsonElement.getAsInt();
        }

        return null;
    }

    /**
     * 获取gson对象的boolean值
     *
     * @param jsonObject
     * @param key
     * @return
     */
    public static Boolean getBooleanValue(JsonObject jsonObject, String key) {

        JsonElement jsonElement = jsonObject.get(key);
        if (jsonElement != null && !jsonElement.isJsonNull()) {

            return jsonElement.getAsBoolean();
        }

        return null;
    }

    /**
     * 获取gson对象的double值
     *
     * @param jsonObject
     * @param key
     * @return
     */
    public static Double getDoubleValue(JsonObject jsonObject, String key) {

        JsonElement jsonElement = jsonObject.get(key);
        if (jsonElement != null && !jsonElement.isJsonNull()) {

            return jsonElement.getAsDouble();
        }

        return null;
    }

    /**
     * 获取gson对象的long值
     *
     * @param jsonObject
     * @param key
     * @return
     */
    public static Long getLongValue(JsonObject jsonObject, String key) {
        JsonElement jsonElement = jsonObject.get(key);
        if (jsonElement != null && !jsonElement.isJsonNull()) {

            return jsonElement.getAsLong();
        }

        return null;
    }

}
