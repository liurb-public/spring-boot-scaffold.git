package org.liurb.springboot.starter.gson.serializer;

import com.google.gson.JsonElement;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import java.lang.reflect.Type;
import java.time.LocalDateTime;
import java.time.ZoneOffset;

/**
 * gson日期序列化类
 *
 * 序列化
 *
 * 统一将LocalDateTime转为毫秒数
 *
 * @Author Liurb
 * @Date 2022/11/30
 */
public class LocalDateTimeGsonSerializer implements JsonSerializer<LocalDateTime> {

    @Override
    public JsonElement serialize(LocalDateTime localDateTime, Type type, JsonSerializationContext jsonSerializationContext) {
        long timestamp = localDateTime.toInstant(ZoneOffset.of("+8")).toEpochMilli();
        return new JsonPrimitive(timestamp);
    }
}

