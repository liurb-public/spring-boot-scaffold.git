package org.liurb.springboot.starter.gson.annotation;

import java.lang.annotation.*;

/**
 * gson解析注解
 *
 * 一般使用于非json格式请求的参数进行拦截注入
 * 需要使用参数解析器进行处理，具体可查看GsonParserArgumentResolver类
 *
 * 应用场景：
 * 1.针对不知道请求方使用何种方式带参数，如支付宝有些回调是使用application/x-www-form-urlencoded
 * 2.对于get请求的参数也可以进行拦截注入到bean中，并且可以配合使用fastjson的JSONField注解进行重命名替换
 * 3.可配合Valid注解使用，对请求参数进行判空等校验
 *
 * @Author Liurb
 * @Date 2022/11/30
 */
@Target({ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface GsonParser {

    boolean require() default true;
}
