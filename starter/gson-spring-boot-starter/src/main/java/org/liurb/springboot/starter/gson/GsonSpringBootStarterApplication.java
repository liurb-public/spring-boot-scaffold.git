package org.liurb.springboot.starter.gson;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GsonSpringBootStarterApplication {

	public static void main(String[] args) {
		SpringApplication.run(GsonSpringBootStarterApplication.class, args);
	}

}
