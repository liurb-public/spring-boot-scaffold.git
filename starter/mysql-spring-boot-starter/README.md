# mysql-spring-boot-starter

## 介绍
* 集成 `mybatis-plus`
* 集成插件 `mybatis-plus-join`，用于联表查询。[官方文档](https://github.com/yulichang/mybatis-plus-join)

## 打包
可先本地进行打包调试，后续可以上传到私服中
```shell
mvn package
mvn install
```

## 使用
本地打包后，在所需项目 `pom.xml` 引入此项目
```xml
        <dependency>
            <groupId>org.liurb.springboot.scaffold</groupId>
            <artifactId>mysql-spring-boot-starter</artifactId>
            <version>${mysql-spring-boot-starter.version}</version>
        </dependency>
```

## 代码生成器
可参考 `GenerateMain` 类，使用build构造方法设置属性值