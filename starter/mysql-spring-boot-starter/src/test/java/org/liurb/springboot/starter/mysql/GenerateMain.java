package org.liurb.springboot.starter.mysql;

import org.liurb.springboot.starter.mysql.mybatis.generate.CodeGenerator;

/**
 * 代码生成器 main
 *
 * @Author LiuRuiBin
 * @Date 2022/11/28
 */
public class GenerateMain {

    private static String jdbcUrl = "";
    private static String jdbcUserName = "";

    private static String jdbcPassword = "";

    private static String daoOutputDirRelate = "";

    private static String mapperOutputDirRelate = "";

    private static String author = "";

    private static String packageName = "";

    private static String moduleName = "";

    private static String tableNames = "";

    private static String tablePrefix = "";


    public static void main(String[] args) {
        CodeGenerator codeGenerator = CodeGenerator.builder().jdbcUrl(jdbcUrl).jdbcUserName(jdbcUserName).jdbcPassword(jdbcPassword)
                .daoOutputDirRelate(daoOutputDirRelate).mapperOutputDirRelate(mapperOutputDirRelate).author(author)
                .packageName(packageName).moduleName(moduleName).tableNames(tableNames).tablePrefix(tablePrefix)
                .build();

        codeGenerator.generator();
    }

}
