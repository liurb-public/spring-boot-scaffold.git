package org.liurb.springboot.starter.mysql.mybatis.generate;

import com.baomidou.mybatisplus.generator.FastAutoGenerator;
import com.baomidou.mybatisplus.generator.config.OutputFile;
import com.baomidou.mybatisplus.generator.engine.FreemarkerTemplateEngine;
import lombok.Builder;
import lombok.Data;

import java.util.Collections;

/**
 * mybatis-plus代码生成器
 *
 * @Author Liurb
 * @Date 2022/11/28
 */
@Data
@Builder
public class CodeGenerator {

    // 数据库连接配置
//    private static final String JDBC_DRIVER = "com.mysql.cj.jdbc.Driver";
//    private static final String JDBC_URL = "jdbc:mysql://domain:3306/database?serverTimezone=UTC&useUnicode=true&characterEncoding=utf-8&useSSL=true&serverTimezone=Asia/Shanghai";
//    private static final String JDBC_USER_NAME = "username";
//    private static final String JDBC_PASSOWRD = "password";

    // 包名和模块名
//    private static final String PACKAGE_NAME = "org.liurb.springboot.starter";
//    private static final String MODULE_NAME = "demo";

    //输出目录
//    private static String projectPath = System.getProperty("user.dir");
//    private static String daoOutputDir = projectPath + "/src/main/java";
//    private static String mapperOutputDir = projectPath + "/src/main/resources/mapper";

    // 表名，多个表使用英文逗号分割
//    private static final String TBL_NAMES = "demo_wechat_account";

    // 表名的前缀，从表生成代码时会去掉前缀
//    private static final String TABLE_PREFIX = "demo_";

    private String jdbcUrl;

    private String jdbcUserName;

    private String jdbcPassword;

    private String daoOutputDirRelate;

    private String mapperOutputDirRelate;

    private String author;

    private String packageName;

    private String moduleName;

    private String tableNames;

    private String tablePrefix;

    // 执行
    public void generator() {

        FastAutoGenerator.create(jdbcUrl, jdbcUserName, jdbcPassword);

        String projectPath = System.getProperty("user.dir");

        String daoOutputDir = projectPath + daoOutputDirRelate;
        String mapperOutputDir = projectPath + mapperOutputDirRelate;


        FastAutoGenerator.create(jdbcUrl, jdbcUserName, jdbcPassword)
                .globalConfig(builder -> {
                    builder.outputDir(daoOutputDir)
                            .author(author)
                            .disableOpenDir();
                })
                .packageConfig(builder -> {
                    builder.parent(packageName)
                            .moduleName(moduleName)
                            .pathInfo(Collections.singletonMap(OutputFile.xml, mapperOutputDir));
                })
                .strategyConfig(builder -> {
                    builder.addInclude(tableNames) // 设置需要生成的表名
                            .addTablePrefix(tablePrefix) // 设置过滤表前缀
                            .entityBuilder() // 设置实体类
                            .enableFileOverride() //实体类覆盖
                            .enableTableFieldAnnotation() //属性加上说明注释
                            .enableLombok() // 使用lombok
                            .serviceBuilder() // 设置服务类
                            .formatServiceFileName("%sService"); //格式化service类

                })
                .templateConfig(builder -> {
                    builder.controller(null); // 不生成controller类
                    builder.xml(null); // 不生成xml
                })
                .templateEngine(new FreemarkerTemplateEngine()// 使用Freemarker引擎模板，默认的是Velocity引擎模板
                )
                .execute();

    }
}
