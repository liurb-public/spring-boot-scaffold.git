package org.liurb.springboot.starter.mysql;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MysqlSpringBootStarterApplication {

	public static void main(String[] args) {
		SpringApplication.run(MysqlSpringBootStarterApplication.class, args);
	}

}
